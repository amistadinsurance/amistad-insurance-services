Amistad Insurance Services is a local, North Carolina based insurance agency. We specialize in providing facilitation services for auto, renters, home, business, life and motorcycle insurance. Furthermore, we understand the complicated and mercurial nature of the personal insurance industry.

Address: 421-114 Chapanoke Rd, Raleigh, NC 27603

Phone: 919-926-7166
